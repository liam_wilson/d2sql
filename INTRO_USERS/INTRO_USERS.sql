/*CREATE DATABASE INTRO_USERS
GO

USE INTRO_USERS
GO*/

--Creates Tables
CREATE TABLE Document (
	ID int NOT NULL PRIMARY KEY,
	Date date NOT NULL,
	Link varchar(30) NOT NULL,
	Type varchar(50) NOT NULL
)

CREATE TABLE ClientType (
	Name varchar(20) NOT NULL PRIMARY KEY,
	Cost float NOT NULL,
	Hours int NOT NULL
)
	
CREATE TABLE Appointment (
	ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Notes varchar(2000)
)

CREATE TABLE TimeSlot (
	ID int NOT NULL IDENTITY(1,1),
	Time varchar(50) NOT NULL,
	Date varchar(50) NOT NULL,
	PRIMARY KEY (ID, Time, Date),
	a_ID int NOT NULL UNIQUE,
	FOREIGN KEY(a_ID) REFERENCES Appointment
)

CREATE TABLE Car (
	License varchar(10) NOT NULL PRIMARY KEY,
	Make varchar(20)
)

CREATE TABLE Client (
	UserName varchar(20) NOT NULL PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	LastName varchar(20) NOT NULL,
	Password varchar(20) NOT NULL,
	Email varchar(50) NOT NULL,
	phone varchar(20) NOT NULL,
	Type varchar(20) NOT NULL,
	FOREIGN KEY (Type) REFERENCES ClientType
)


CREATE TABLE Admin (
	UserName varchar(20) NOT NULL PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	LastName varchar(20) NOT NULL,
	Email varchar(50) NOT NULL,
	Password varchar(20) NOT NULL
)

CREATE TABLE Instructor (
	UserName varchar(20) NOT NULL PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	LastName varchar(20) NOT NULL,
	Email varchar(50) NOT NULL,
	Password varchar(20) NOT NULL,
	Phone varchar(10) NOT NULL
)

CREATE TABLE Books (
	c_UserName varchar(20) NOT NULL,
	a_ID int NOT NULL,
	PRIMARY KEY (c_UserName, a_ID),
	FOREIGN KEY(c_UserName) REFERENCES Client,
	FOREIGN KEY(a_ID) REFERENCES Appointment
)

CREATE TABLE Assigned (
	a_ID int NOT NULL,
	License varchar(10) NOT NULL,
	i_UserName varchar(20) NOT NULL,
	Confirmed bit NOT NULL,
	PRIMARY KEY (a_ID, License, i_UserName),
	FOREIGN KEY(a_ID) REFERENCES Appointment,
	FOREIGN KEY(License) REFERENCES Car,
	FOREIGN KEY(i_UserName) REFERENCES Instructor
)

CREATE TABLE Receives (
	c_UserName varchar(20) NOT NULL,
	d_ID int NOT NULL,
	PRIMARY KEY (c_UserName, d_ID),
	FOREIGN KEY(c_UserName) REFERENCES Client,
	FOREIGN KEY(d_ID) REFERENCES Document
)

--Drop Tables
DROP TABLE Document
DROP TABLE ClientType
DROP TABLE Appointment
DROP TABLE TimeSlot
DROP TABLE Car
DROP TABLE Client
DROP TABLE Admin
DROP TABLE Instructor
DROP TABLE Books
DROP TABLE Assigned
DROP TABLE Receives

--Inserts Data into Tables
INSERT INTO Car VALUES ('XWQ231', 'Toyota')
INSERT INTO Car VALUES ('GES223', 'Nissan')
INSERT INTO Car VALUES ('F4ST3R', 'Nissan')
INSERT INTO Car VALUES ('TDS231', 'Holden')
INSERT INTO Car VALUES ('WJC212', 'Lancia')

INSERT INTO ClientType VALUES ('Beginner', 150.00, 10)
INSERT INTO ClientType VALUES ('Intermediate', 200.00, 15)
INSERT INTO ClientType VALUES ('Advanced', 250.00, 20)

INSERT INTO Instructor VALUES ('jimbilly1', 'Jim', 'Billy', 'jimbilly1@learntodrive.com', 'pass1', '0277544124')
INSERT INTO Instructor VALUES ('nevilrodgers1', 'Nevil', 'Rodgers', 'nevilrodgers1@learntodrive.com', 'pass2', '0277154879')
INSERT INTO Instructor VALUES ('benwalker1', 'Ben', 'Walker', 'benwalker1@learntodrive.com', 'pass3', '0271234658')
INSERT INTO Instructor VALUES ('loganlett1', 'Logan', 'Lett', 'loganlett1@learntodrive.com', 'pass4', '0274721548')

INSERT INTO Admin VALUES ('rubberbruin1', 'Rubber', 'Bruin', 'rubberbruin1@learntodrive.com', 'pass5')
INSERT INTO Admin VALUES ('janakadhikari1', 'Janak', 'Adhikari', 'janakadhikari1@learntodrive.com', 'pass6')
INSERT INTO Admin VALUES ('williamrussel1', 'William', 'Russel', 'williamrussel1@learntodrive.com', 'pass7')


INSERT INTO Appointment VALUES ('work on cornering')
INSERT INTO Appointment VALUES ('work on cornering')
INSERT INTO Appointment VALUES ('work on paralel parking')
INSERT INTO Appointment VALUES ('work on paralel parking')
INSERT INTO Appointment VALUES ('work on cornering')
INSERT INTO Appointment VALUES ('work on lane changing')
INSERT INTO Appointment VALUES ('work on paralel parking')
INSERT INTO Appointment VALUES ('work on cornering')
INSERT INTO Appointment VALUES ('work on cornering')
INSERT INTO Appointment VALUES ('work on lane changing')
INSERT INTO Appointment VALUES ('wwork on lane changing')
INSERT INTO Appointment VALUES ('work on cornering')
INSERT INTO Appointment VALUES ('work on cornering')
INSERT INTO Appointment VALUES ('no notes')
INSERT INTO Appointment VALUES ('no notes')
INSERT INTO Appointment VALUES ('no notes')
INSERT INTO Appointment VALUES ('work on lane changing')
INSERT INTO Appointment VALUES ('work on lane changing')
INSERT INTO Appointment VALUES ('work on indicating')
INSERT INTO Appointment VALUES ('work on fast speeds')
INSERT INTO Appointment VALUES ('work on lane changing')
INSERT INTO Appointment VALUES ('work on fast speeds')
INSERT INTO Appointment VALUES ('work on fast speeds')
INSERT INTO Appointment VALUES ('work on lane changing')
INSERT INTO Appointment VALUES ('no notes')
INSERT INTO Appointment VALUES ('no notes')
INSERT INTO Appointment VALUES ('work on lane changing')
INSERT INTO Appointment VALUES ('no notes')
INSERT INTO Appointment VALUES ('no notes')
INSERT INTO Appointment VALUES ('no notes')
INSERT INTO Appointment VALUES ('no notes')

INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (1, '8.00am', '2017/03/01')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (3, '10.30am', '2017/03/02')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (10, '1.30pm', '2017/03/03')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (7, '4.00pm', '2017/03/04')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (2, '2.30pm', '2017/03/05')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (13, '2.00pm', '2017/03/06')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (21, '9.30am', '2017/03/07')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (24, '8.00am', '2017/03/08')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (4, '3.30pm', '2017/03/09')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (12, '1.30pm', '2017/03/10')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (6, '12.30pm', '2017/03/11')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (8, '1.00pm', '2017/03/12')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (23, '2.00pm', '2017/03/13')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (18, '8.30am', '2017/03/14')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (22, '10.00am', '2017/03/15')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (19, '11.00am', '2017/03/16')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (14, '2.30pm', '2017/03/17')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (5, '3.30pm', '2017/03/18')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (15, '8.30am', '2017/03/19')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (9, '9.00am', '2017/03/20')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (20, '10.30am', '2017/03/21')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (16, '2.30pm', '2017/03/22')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (11, '8.00am', '2017/03/23')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (17, '8.30am', '2017/03/24')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (28, '10.30am', '2017/03/25')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (26, '2.30pm', '2017/03/26')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (29, '4.30pm', '2017/03/27')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (27, '2.00pm', '2017/03/28')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (30, '11.30am', '2017/03/29')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (25, '8.30am', '2017/03/30')
INSERT INTO TimeSlot (a_ID, Time, Date) VALUES (31, '9.00am', '2017/03/31')



--select statement
SELECT * FROM Car
SELECT * FROM ClientType
SELECT * FROM Instructor
SELECT * FROM Admin
SELECT * FROM Appointment
SELECT * FROM TimeSlot 

SELECT * FROM Client
